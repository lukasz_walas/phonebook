<?php
include "connectDb.php";
$result = $db->query("SELECT * FROM $db_name");
$result_array = $result->fetch(PDO::FETCH_ASSOC);

if($result_array) {
    $table_key = array_keys($result_array);
    $fields_num = count($result_array);
    echo "<table class = 'table table-bordered table-striped'><tr>";
    for ($i = 0; $i < $fields_num; $i++) {
        echo "<th>{$table_key[$i]}</th>";
    }
    echo "</tr>\n";

    do {
        echo "<tr>";
        echo "<td>{$result_array['id']}</td>";
        echo "<td>{$result_array['imie']}</td>";
        echo "<td>{$result_array['nazwisko']}</td>";
        echo "<td>{$result_array['numer']}</td>";
        echo '<td><a href="index.php?action=edit&id=' .$result_array['id']. '" class="btn btn-primary">Edytuj</a></td>';
        echo '<td><a href="index.php?action=delete&id=' . $result_array['id'] . '" class="btn btn-danger">Usuń</a></td>';
        echo "</tr>\n";
    } while ($result_array = $result->fetch(PDO::FETCH_ASSOC));

    echo "</table>";
}
echo "<a href='index.php?action=add'class='btn btn-primary'>Dodaj Kontakt</a>";

include 'htmlFooter.php';

?>
<?php
include 'htmlHead.php';
include 'connectDb.php';
include 'function.php';

switch ($_GET['action']) {
    case 'delete':
        deleteContact($db,$db_name,$_GET['id']);
        include 'backToListButton.html.php';
        break;
    case 'list':
        include 'list.html.php';
        break;
    case 'add':
        if (isset($_POST['name'])& isset($_POST['surname']) & isset($_POST['number'])){
            addContat($db,$db_name,$_POST['name'],$_POST['surname'],$_POST['number']);
            include 'backToListButton.html.php';
        }else {
            include 'addContactView.html.php';
        }
        break;
    case 'edit':
        if (isset($_POST['name'])& isset($_POST['surname']) & isset($_POST['number'])) {
            changeContat($db,$db_name,$_POST['name'],$_POST['surname'],$_POST['number'],$_GET['id']);
            include 'backToListButton.html.php';
        }else{
            $row = returnOldValue($db, $db_name, $_GET['id']);
            include 'edit.html.php';

        }
}
include 'htmlFooter.php';

?>